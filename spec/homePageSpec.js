var request = require("request");

var base_url = "http://localhost:3000/"

describe("Home Page Server", function() {
  describe("GET /", function() {

    it("returns status code 200", function(done) {
      request.get(base_url, function(error, response, body) {
        expect(response.statusCode).toBe(200);
        done();
      });
    });

    it("Page Contains text[Imagine A Company] ", function(done) {
      request.get(base_url, function(error, response, body) {
        expect(body).toContain("Imagine-a-Company");
        done();
      });
    });

  });
});
