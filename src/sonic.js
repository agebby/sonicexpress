var express = require('express');
var app = express();
var exphbs = require('express3-handlebars');
exphbs.ExpressHandlebars.prototype.layoutsDir = __dirname + '/views/layouts';

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
// static file resolution
app.use('/assets',express.static(__dirname + '/public'));


app.get('/', function(req, res) {
  res.render('index');
});
app.get('/about', function(req, res) {
  var fortunes = [
    "Conquer your fears or they will conquer you.",
    "Rivers need springs.",
    "Do not fear what you don't know.",
    "You will have a pleasant surprise.",
    "Whenever possible, keep it simple.",
  ];
  var randomFortune = fortunes[Math.floor(Math.random() * fortunes.length)];
  var luckyNumber = Math.round( Math.random() * 100 );
  res.render('about/index', {
    luckyNumber : luckyNumber,
    fortune : randomFortune
  });
});

app.get('/about/contact',function(req,res){
  res.render('about/contact');
});

app.get('/about/directions',function(req,res){
  res.render('about/directions');
});

// custom 404 page
app.use(function(req, res) {
  res.status(404);
  res.render('404');
});

// custom 500 page
app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.type('text/plain');
  res.status(500);
  res.send('500 - Server Error');
});
app.listen(app.get('port'), function() {
  console.log('Express started on http://localhost:' +
    app.get('port') + '; press Ctrl-C to terminate.');
});
