var http = require('http'),
  fs = require('fs');

function serveStaticFile(res, path, contentType, responseCode) {
  if (!responseCode) responseCode = 200;
  fs.readFile(__dirname + path, function(err, data) {
    if (err) {
      res.writeHead(500, {
        'Content-Type': 'text/plain'
      });
      res.end('500 - Internal Error');
    } else {
      res.writeHead(responseCode, {
        'Content-Type': contentType
      });
      res.end(data);
    }
  });
}




http.createServer(function(req, res) {
  // normalize url by removing querystring, optional // trailing slash, and making lowercase
  var path = req.url.replace(/\/?(?:\?.*)?$/, '')
    .toLowerCase();
  switch (path) {
    case '':
      serveStaticFile(res, '/public/index.html', 'text/html');
      break;
    case '/about':
      serveStaticFile(res, '/public/about.html', 'text/html');
      break;

    case '/images/logo.png':
      serveStaticFile(res, '/public/images/logo.png', 'image/png');
      break;

    case '/images/image.png':
      serveStaticFile(res, '/public/images/image.png', 'image/png');
      break;

    case '/images/square-image.png':
      serveStaticFile(res, '/public/images/square-image.png', 'image/png');
      break;

    case '/images/wireframe.png':
      serveStaticFile(res, '/public/images/wireframe.png', 'image/png');
      break;
    case '/css/semantic.css':
      serveStaticFile(res, '/public/css/semantic.css', 'text/css');
      break;
    case '/css/themes/default/assets/fonts/icons.woff2':
      serveStaticFile(res, '/public/css/themes/default/assets/fonts/icons.woff2', 'text/css');
      break;
    case '/css/themes/default/assets/fonts/icons.woff ':
      serveStaticFile(res, '/public/css/themes/default/assets/fonts/icons.woff ', 'text/css');
      break;
    case '/css/themes/default/assets/fonts/icons.ttf ':
      serveStaticFile(res, '/public/css/themes/default/assets/fonts/icons.ttf ', 'text/css');
      break;
    case '/js/semantic.js':
      serveStaticFile(res, '/public/js/semantic.js', 'text/javascript');
      break;
    default:
      serveStaticFile(res, '/public/404.html', 'text/html', 404);
      break;
  }
}).listen(3000);
console.log('Server started on localhost:3000; press Ctrl-C to terminate....');
