## Sonic Express

This is a simple music search app to learn MEAN stack

### Usage
- Clone or fork this repository
- Make sure you have [node.js](https://nodejs.org/) installed
- run `npm install` to install dependencies
- run `npm start` to fire up dev server


### To Debug tests

- run node inspector : node_modules/node-inspector/bin/inspector.js &
- run tests  :  node --debug-brk node_modules/jasmine-node/lib/jasmine-node/cli.js spec
- open chrome to: http://0.0.0.0:8080/debug?port=5858



http://www.tikalk.com/js/debugging-jasmine-node-tests/

https://github.com/node-inspector/node-inspector#quick-start

https://github.com/leonidas/transparency/wiki/Debugging-and-running-spec-tests
